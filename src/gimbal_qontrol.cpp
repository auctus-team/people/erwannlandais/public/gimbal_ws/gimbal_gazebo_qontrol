#include <gimbal_gazebo_qontrol/gimbal_qontrol.h>

namespace gimbal_qp_controller
{
    // rosservice pr publisher pose_init ds world

    VelocityController::VelocityController(ros::NodeHandle& n, double period, std::string urdfPath, bool withGazebo):
    tU(n, false)
    {
        this->nh = n;
        this->period = period;
        //ROS_WARN_STREAM_ONCE("Initializing gimbal QP Controller");

        // ros_init

        std::string nodeName = ros::this_node::getName();
        nodeName += "/";
        std::string varPath;
        rosWrap::getRosParam(nodeName + "varPath", varPath);
        varPath+="/";

        std::string model_name;

        bool needHome = false;
        std::string homePath = "";
        rosWrap::getRosParam(varPath + "needHomeToPath", needHome);
        rosWrap::getRosParam(varPath + "gammaMainTask", gamma);

        if (needHome)
        {
            std::string s = "HOME";
            homePath = rosWrap::getEnvVar(s);
            homePath += "/";
        }

        //std::cout << homePath << std::endl;



        rosWrap::getRosParam(nodeName + "sim", isSim);

        rosWrap::getRosParam(varPath + "controlled_frame", controlled_frame);
        base_frame="world";
        rosWrap::getRosParam(varPath + "base_frame", base_frame,1);

        // rosWrap::getRosParam(varPath + "urdf_path", urdf_path);
        // urdf_path = homePath + urdf_path;

        urdf_path = homePath + urdfPath;

        rosWrap::getRosParam(varPath + "model", model_name);

        rosWrap::getRosParam(varPath + "mainTaskType", mTChoice);

        std::cout << "init : chosen choice : " << mainTaskType[mTChoice] << std::endl;

        this->withGazebo = withGazebo;


        std::vector<double> p_gains_joint;
        p_gains_joint.resize(3);
        rosWrap::getRosParam(varPath+"p_gains_joint", p_gains_joint);

        std::vector<double> p_gains_cart;
        p_gains_cart.resize(3);
        rosWrap::getRosParam(varPath+"p_gains_cart", p_gains_cart);


        for (int i = 0 ; i < p_gains_joint.size();i++)
        {
            pGainsJ[i] = p_gains_joint[i];
        }
        for (int i = 0 ; i < p_gains_cart.size();i++)
        {
            pGainsC[i] = p_gains_cart[i];
        }

        std::vector<double> uBnd;
        uBnd.resize(3);
        rosWrap::getRosParam(varPath+"q_upper", uBnd);
        std::vector<double> lBnd;
        lBnd.resize(3);
        rosWrap::getRosParam(varPath+"q_lower", lBnd);

        for (int i = 0; i < uBnd.size(); i++)
        {
            // upperQBounds[i] = uBnd[i];
            // lowerQBounds[i] = lBnd[i];
            upperQBounds[i] = uBnd[i];
            lowerQBounds[i] = lBnd[i];            
        }

        //std::cout << upperQBounds << std::endl;

        rosWrap::getRosParam(varPath+"vel_constraint_max", maxSpeedPercent);

        rosWrap::getRosParam(varPath+"acc_constraint_max", maxAccPercent);
        rosWrap::getRosParam(varPath+"reg_task_choice", regChoice);

        rosWrap::getRosParam(varPath+"distQPRec", distQPRec);


        accLimsOri.resize(3);
        for (int i = 0; i <3; i++)
        {
            accLimsOri(i,0) = 24.0;
        }


        model_name = "/"+model_name+"/";

        rosWrap::getRosParam(varPath + "cob_task_weight", saved_cob_task_weight);
        rosWrap::getRosParam(varPath + "vM_task_weight", saved_vM_task_weight);

        rosWrap::getRosParam(varPath + "simu_time_factor", simu_time_factor,1);


        init_qp();

        ROS_INFO("QP DONE");

        template_js_msg = sensor_msgs::JointState();

        std::vector<std::string> names = model->getJointNames();
        int numJoints = names.size()-1;
        template_js_msg.position.resize(numJoints);
        template_js_msg.velocity.resize(numJoints);
        template_js_msg.effort.resize(numJoints);     
        lastDesiredJointConfig.resize(numJoints);
        lastDesiredJointSpeed.resize(numJoints);
        lastSavedConfigForRecover.resize(numJoints);
        lastSavedConfigForRecover.setOnes();
        lastSavedConfigForRecover = lastSavedConfigForRecover * -999999;

        for (int i = 1; i < names.size(); i++)
        {
            template_js_msg.name.push_back(names[i]);
            ROS_INFO("Name %d : %s",i, names[i].c_str());
            //std::cout << "name " << i << " : " << names[i] << std::endl;
        }

        robot_state.resize(numJoints);

        template_js_msg.header.frame_id = "joint-space";

        jsc_msg = template_js_msg;
        js_msg = template_js_msg;    

        jsr_msg = template_js_msg;  

        if (isSim)
        {
            if (!withGazebo)
            {
                // create publisher for current articular state
                joint_state_publisher = this->nh.advertise<sensor_msgs::JointState>("joint_states", 1);

                // create publisher for articular command
                command_state_publisher = this->nh.advertise<sensor_msgs::JointState>(model_name+"desired_joint_states", 1);     

            }
            else
            {
                // create subscriber for current articular state
                joint_state_subscriber = this->nh.subscribe(model_name+"joint_states", 1, &VelocityController::getCurrentJointState, this);

                // create publisher to access Gazebo controller
                joint_velocity_command_pub = this->nh.advertise<std_msgs::Float64MultiArray>(model_name+"/joint_group_vel_controller/command", 1000);                

            }            
        }
        else
        {
            joint_state_subscriber = this->nh.subscribe("/joint_states", 1, &VelocityController::getCurrentJointState, this);

            
            // create publisher for articular command
            command_state_publisher = this->nh.advertise<sensor_msgs::JointState>("/desired_joint_states", 1);     

                // create publisher to access Gazebo controller
                //joint_velocity_command_pub = this->nh.advertise<std_msgs::Float64MultiArray>(model_name+"/joint_group_vel_controller/command", 1000);                

        }


        //joint_state_subscriber = this->nh.subscribe(model_name+"joint_states", 1, &VelocityController::getCurrentJointState, this);

        

        // create subscriber for cartesian command
        cartesian_command_subscriber = this->nh.subscribe("/PoseVelAcc", 1, &VelocityController::getCartCommand, this);

        // create subscriber for joint command
        joint_command_subscriber = this->nh.subscribe("/JSD", 1, &VelocityController::getJointCommand, this);


        // create error publisher
        QP_error_pub = this->nh.advertise<msg_srv_action_gestion::poseVelAcc>("/QP_errors", 1); 


        // serviceServer for main task switch (float64)
        switchControlServer = this->nh.advertiseService("/set_control_type",  &VelocityController::setControlType,this);    

        // serviceServer for reg task switch 
        // returns : the previously selected task
        switchRegTaskServer = this->nh.advertiseService("/set_reg_task",  &VelocityController::setRegTask,this);    

        // serviceServer for trackMvt (setBool)
        trackMvtServer = this->nh.advertiseService("/set_track_mvt",  &VelocityController::setTrackMvt,this);    

        // serviceServer for poseInit (doublefloat64)
        poseInitServer = this->nh.advertiseService("/get_pose_init",  &VelocityController::sendPoseInit,this);    

        // serviceServer for "initialized" (getBool)
        initializedServer = this->nh.advertiseService("/qp_is_initialized",  &VelocityController::isQPInitialized,this);    

        

        gotState = false;     


    }

    void VelocityController::init_qp()
    {

        ROS_INFO("CREATE MODEL FROM FILE...");
              model =
          Model::RobotModel<Model::RobotModelImplType::PINOCCHIO>::loadModelFromFile(urdf_path,base_frame,controlled_frame);
        ROS_INFO("MODEL done...");
    // test
        auto solver =
      std::make_shared<Solver::Solver<Solver::SolverImplType::qpmad>>();
      velocity_problem = std::make_shared<Qontrol::JointVelocityProblem>(model,solver);  


    //velocity_problem = std::make_shared<Qontrol::JointVelocityProblem>(model);  

      //main_task = velocity_problem->task_set->add<Task::CartesianVelocity>("MainTask"); 
      main_task = velocity_problem->task_set->add("CustomMainTask", 3, 1.0);      

      std::string regTaskName;
      regTaskName = regPoss[regChoice];

      if (regChoice == 0)
      {
        // vM
        cob_task_weight = 0.0;
        vM_task_weight = saved_vM_task_weight;
      }
      else if (regChoice == 1)
      {
        // coB
        cob_task_weight =  saved_cob_task_weight;
        vM_task_weight = 0.0;
      }



      config_reg_task_cob = velocity_problem->task_set->add("MiddleOfConfigBounds", 3, cob_task_weight);   
      config_reg_task_vM = velocity_problem->task_set->add("VelocityMinimisation", 3, vM_task_weight);   

      //auto regularisation_task = velocity_problem->task_set->add<Task::JointVelocity>("RegularisationTask",reg_task_weight); 
      speedLims = maxSpeedPercent*model->getJointVelocityLimits();

      accLims = maxAccPercent*accLimsOri;

      model->setJointVelocityLimits(speedLims);
      model->setLowerJointPositionLimits(lowerQBounds);
      model->setUpperJointPositionLimits(upperQBounds);

      joint_configuration_constraint = velocity_problem->constraint_set->add<Constraint::JointConfiguration>("JointConfigurationConstraint");
    
    //   joint_configuration_constraint->setUpperBounds(upperQBounds);
    //   joint_configuration_constraint->setLowerBounds(lowerQBounds);

      joint_velocity_constraint = velocity_problem->constraint_set->add<Constraint::JointVelocity>("JointVelocityConstraint");

      joint_acceleration_constraint = velocity_problem->constraint_set->add("JointAccelerationConstraint",3);

    //   joint_velocity_constraint->setUpperBounds(speedLims);
    //   joint_velocity_constraint->setLowerBounds(-speedLims);

        resetQP = false;


    }

    bool VelocityController::initialize()
    {
        ROS_WARN_STREAM_ONCE("Initializing gimbal QP Controller");

        if (!initialized) 
        {        
            int nJ = model->getNrOfDegreesOfFreedom();

            // init jsr_msg
            for (int i=0; i<model->getNrOfDegreesOfFreedom() ; ++i)
            {
                robot_state.joint_position[i] = lastDesiredJointConfig[i];
                robot_state.joint_velocity[i] =  lastDesiredJointSpeed[i];

                // lastDesiredJointConfig[i] = jsr_msg.position[i];
                // lastDesiredJointSpeed[i] = jsr_msg.velocity[i];    

                // std::cout << i << " ok" << std::endl;
            }
            model->setRobotState(robot_state);

            lastDesiredCartesianPose = model->getFramePose(model->getTipFrameName()).matrix();
            lastDesiredCartesianVelocity.setZero();

            poseInit = lastDesiredCartesianPose;

            std::cout << lastDesiredJointConfig << std::endl;
            std::cout << lastDesiredCartesianPose << std::endl;

            ROS_WARN_STREAM("Initialized qp");
            initialized = true;

        }
        ROS_WARN_STREAM("gimbal_QP_Control initialized");
        return true;
    }

    void VelocityController::update()
    {
        if (resetQP)
        {
            init_qp();
            resetQP = false;
        }

        if (resetReg)
        {
            if (regChoice == 0)
            {
                // vM
                cob_task_weight = 0.0;
                vM_task_weight = saved_vM_task_weight;
            }
            else if (regChoice == 1)
            {
                // coB
                cob_task_weight =  saved_cob_task_weight;
                vM_task_weight = 0.0;
            }    
            //velocity_problem->task_set->setTaskWeight("MiddleOfConfigBounds",cob_task_weight);
            //velocity_problem->task_set->setTaskWeight("VelocityMinimisation",vM_task_weight);        
            velocity_problem->task_set->setTaskWeight(config_reg_task_cob,cob_task_weight);
            velocity_problem->task_set->setTaskWeight(config_reg_task_vM,vM_task_weight);        
            
            
            resetReg = false;
        }

        js_msg.position.clear();
        js_msg.velocity.clear();
        int nJ = model->getNrOfDegreesOfFreedom();

        for (int i=0; i<model->getNrOfDegreesOfFreedom() ; ++i)
        {
            robot_state.joint_position[i] = jsr_msg.position[i];
            robot_state.joint_velocity[i] = jsr_msg.velocity[i];

            // robot_state.joint_position[nJ-i-1] = jsr_msg.position[i];
            // robot_state.joint_velocity[nJ - i - 1] = jsr_msg.velocity[i];            

            // update js msg
            js_msg.position[i] = jsr_msg.position[i];
            js_msg.velocity[i] = jsr_msg.velocity[i];

        //ROS_INFO("state for joint %d : name : %s :  pos : %3.4f ; speed : %3.4f",i, jsr_msg.name[i].c_str(),robot_state.joint_position[i],robot_state.joint_velocity[i]  );

        }

        manageROSCalls();


        //std::cout << "set_up ok" << std::endl;


        updateQP( lastDesiredCartesianPose,  lastDesiredCartesianVelocity, 
        lastDesiredJointConfig, lastDesiredJointSpeed,
        period);

        if (qpSuccess)
        {
            // can do special stuff here

            // reset lastSavedConfig
            lastSavedConfigForRecover.setOnes();
            lastSavedConfigForRecover = lastSavedConfigForRecover * -999999;            

        }
        // set infos for publishing
        publishInfos(js_msg,jsc_msg);


    }

    void VelocityController::manageROSCalls()
    {
        if ( chosenMainTask != -1)
        {
            mTChoice = chosenMainTask;
            chosenMainTask = -1;
        }
    }

    void VelocityController::QPRecovery(Eigen::VectorXd & jVC, Eigen::Matrix4d desiredCartesianPose, Eigen::Matrix<double,6,1> desiredCartesianVelocity, Eigen::VectorXd qdes,
    Eigen::VectorXd qd_des,
    double dt)
    {
        // qualify kind of necessary recovery

        // = -1 : not determined. Solution ==> allSpeeds at zero
        int recovType = -1;

        // = 0 : outOfBounds
        int nJ = model->getNrOfDegreesOfFreedom();
        
        for (int i = 0; i < nJ; i++)
        {
            if (robot_state.joint_position[i] > upperQBounds[i] ||robot_state.joint_position[i] < lowerQBounds[i])
            {
                recovType = 0;
            }
        }

        // = 1 : out of speed bounds
        if (recovType == -1)
        {
            for (int i = 0; i < nJ; i++)
            {
                if ( abs(jVC[i]) > speedLims[i])
                {
                    recovType = 1;
                }
            }
        }

        switch(recovType)
        {
            case -1:
            {
                ROS_INFO("Default case management (==> 0 speed)");
                qpSuccess = true;
                jVC.setZero();
                break;
            }

            case 0:
            {

                ROS_INFO("Case 0 management (out of bounds)");
           
                // gives filtrated command
                Qontrol::RobotState valid_robot_state = robot_state;

                Eigen::VectorXd validConfig = qdes;
                Eigen::VectorXd validSpeed = qd_des;
                Eigen::MatrixXd validPose;
                Eigen::Matrix<double,6,1> validCartSpeed;

                Eigen::VectorXd maxBounds = upperQBounds;
                Eigen::VectorXd minBounds = lowerQBounds;

                for (int i  = 0; i < nJ; i++)
                {
                    if (robot_state.joint_position[i] > upperQBounds[i] )
                    {
                        if ( abs(lastSavedConfigForRecover[i]) < abs(upperQBounds[i]) )
                        {
                            validConfig[i] = lastSavedConfigForRecover[i];
                        }
                        else
                        {
                            validConfig[i] = upperQBounds[i] - distQPRec;

                        }
                        maxBounds[i] = robot_state.joint_position[i]+1e-2;                        
                    }
                    else if (robot_state.joint_position[i] < lowerQBounds[i] )
                    {
                        if ( abs(lastSavedConfigForRecover[i]) < abs(lowerQBounds[i]) )
                        {
                            validConfig[i] = lastSavedConfigForRecover[i];
                        }              
                        else
                        {          
                            validConfig[i] = lowerQBounds[i] +distQPRec;
                        }
                        minBounds[i] = robot_state.joint_position[i]-1e-2;  

                    }       
                    else
                    {
                        validConfig[i] = robot_state.joint_position[i];

                    }   
                    valid_robot_state.joint_position[i] = validConfig[i];
                    ROS_INFO("valid state for joint %d : name : %s :  pos : %3.4f (instead of : %3.4f); speed : %3.4f",i, jsr_msg.name[i].c_str(),valid_robot_state.joint_position[i], robot_state.joint_position[i],valid_robot_state.joint_velocity[i]  );

                }
                if (mainTaskType[mTChoice]== "cartesian")
                {
                    model->setRobotState(valid_robot_state);
                    validPose = model->getFramePose(model->getTipFrameName()).matrix();
                    validCartSpeed.setZero();      
                    //model->setRobotState(robot_state);              
                }
                else if (mainTaskType[mTChoice]== "articular")
                {
                    validSpeed.setZero();
                }                
                // restart with higher bounds
                model->setLowerJointPositionLimits(minBounds);
                model->setUpperJointPositionLimits(maxBounds);

                // computes
                qpSuccess = false;
                updateQP(validPose, validCartSpeed, validConfig, validSpeed,dt);
                ROS_INFO("Computes valid QP; has it worked? %d", qpSuccess);

                if (qpSuccess)
                {
                    lastSavedConfigForRecover = validConfig;
                }
                
                // recompress bounds
                model->setLowerJointPositionLimits(lowerQBounds);
                model->setUpperJointPositionLimits(upperQBounds);
                // to prevent a new modification
                qpSuccess = false;
                // conclude
                break;
            }

            case 1:
            {
                for (int i = 0; i < nJ; i++)
                {
                    if (jVC[i] > speedLims[i])
                    {
                        jVC[i] = speedLims[i];
                    }
                    else if (jVC[i] < -speedLims[i])
                    {
                        jVC[i] = -speedLims[i];
                    }
                }
                qpSuccess = true;
                break;
            }
        }
        

        
    }

    void VelocityController::updateQP(Eigen::Matrix4d desiredCartesianPose, Eigen::Matrix<double,6,1> desiredCartesianVelocity,
    Eigen::VectorXd qdes,
    Eigen::VectorXd qd_des,
    
    double dt)
    {

        int nJ = model->getNrOfDegreesOfFreedom();
        Eigen::MatrixXd I(nJ,nJ); I.setIdentity();

        pinocchio::SE3 oMdes(desiredCartesianPose);    
        //std::cout << robot_state << std::endl;
        model->setRobotState(robot_state);

        pinocchio::SE3 current_pose(model->getFramePose(model->getTipFrameName()).matrix());
                
        
        // for acc
        joint_acceleration_constraint->setConstraintMatrix(Eigen::MatrixXd::Identity(nJ,nJ ) );
        joint_acceleration_constraint->setUpperBounds(robot_state.joint_velocity +dt*accLims);
        joint_acceleration_constraint->setLowerBounds(robot_state.joint_velocity -dt*accLims);
        
        Eigen::Vector3d q;
        for (int i = 0; i < 3; i++)
        {
            q[i] = robot_state.joint_position[i];
        }
        double weight = 0.0;
        // cartesian MainTask
        if (mainTaskType[mTChoice]== "cartesian")
        {
            // bien normal que jacobienne soit de base exprimée en LOCAL
            // car erreur est exprimée en local
            // A temre, tipMdes = R^{fEk}_{oTk}
            // tip : end effector : oTk
            // des : TF désirée : fEk
            const pinocchio::SE3 tipMdes = current_pose.actInv(oMdes);
            auto err = pinocchio::log6(tipMdes).toVector();

            auto ang_err = err.block<3,1>(3,0);
            Eigen::Matrix<double,3,1> xd_star = pGainsC.cwiseProduct(ang_err) + desiredCartesianVelocity.block<3,1>(3,0);

            if (mainTaskLocal)
            {
                // on entre bien ici, OK
                Eigen::Matrix<double,3,3> J = model->getJacobian(model->getTipFrameName()).block<3,3>(3,0);
                double detJ = J.determinant();
                double weight = 1 - exp(-detJ*gamma);
                J(0,2) = J(0,2)*1.0*weight;
                J(1,2) = J(1,2)*1.0*weight;  
                J(2,0) = J(2,0)*1.0*weight; 
                J(2,1) = J(2,1)*1.0*weight; 
                main_task->setE(J);
            }
            else
            {
                main_task->setE(model->getJacobian(model->getTipFrameName()).block<3,3>(3,0));
            }
            
            main_task->setf(xd_star);    

            // std::cout << ang_err << std::endl;
            // std::cout << "===" << std::endl;


            error_msg.pose.orientation.x = ang_err(0,0);
            error_msg.pose.orientation.y = ang_err(1,0);
            error_msg.pose.orientation.z = ang_err(2,0);      
            error_msg.twist.angular.x = xd_star(0,0);
            error_msg.twist.angular.y = xd_star(1,0);   
            error_msg.twist.angular.z = xd_star(2,0);       
        }     

        else if (mainTaskType[mTChoice]== "articular")
        {
            auto ang_err = qdes-q;


            Eigen::Matrix<double,3,1> xd_star = pGainsJ.cwiseProduct(ang_err) + qd_des;

            // articular MainTask
            main_task->setE(I);
            main_task->setf(xd_star);                
            error_msg.pose.orientation.x = ang_err(0,0);
            error_msg.pose.orientation.y = ang_err(1,0);
            error_msg.pose.orientation.z = ang_err(2,0);      
            error_msg.twist.angular.x = xd_star(0,0);
            error_msg.twist.angular.y = xd_star(1,0);   
            error_msg.twist.angular.z = xd_star(2,0);       
        }


                // set reg task as middle of boundaries
        Eigen::Vector3d q_mean; q_mean.setZero();




        config_reg_task_cob->setE( I );
        config_reg_task_vM->setE( I );
        Eigen::VectorXd O(3,1); O.setZero();

        // for center of bounds
        config_reg_task_cob->setf( (1-weight)*(q_mean-q) );
        // for velocity minimization
        config_reg_task_vM->setf( O );

         

        // if (regChoice == 1)
        // {
        //     // for center of bounds
        //     config_reg_task_cob->setf( (1-weight)*(q_mean-q) );
        // }
        // // for velocity minimization
        // else if (regChoice == 0)
        // {
        //     config_reg_task_vM->setf( O );
        // }

        qpSuccess = true;
        Eigen::VectorXd jVC; jVC.resize(nJ);
        try
        {
            velocity_problem->update(dt);
            qpSuccess = velocity_problem->solutionFound();
            jVC = velocity_problem->getJointVelocityCommand();
            // if (!qpSuccess)
            // {
            //     std::cout << "qpFailed" << std::endl;
            // }
            //                     if (mainTaskType[mTChoice]== "articular")
            //         {
            // std::cout << jVC << std::endl;
            // std::cout << "==" << std::endl;
            //         }
            // if (jVC.isZero() )
            // {
            //     std::cout << 
            // }
            // if (mainTaskType[mTChoice]== "articular")
            // {
            //             std::cout << "bug : too big speed" << std::endl;
            //  }
            
        }
        catch(const std::exception& e)
        {
            std::cerr << e.what() << '\n';
            QPRecovery(jVC,desiredCartesianPose, desiredCartesianVelocity, qdes, qd_des, dt);
            // qpSuccess = true;
            // jVC.setZero();
            // if works : we can add recovery steps 
        }
        
        if (qpSuccess)
        {

            jsc_msg.velocity.clear();
            jsc_msg.position.clear();
            double dt_val = dt;

            if (isSim)
            {
                dt_val = simu_time_factor*dt;
            }


            for (int i = 0; i < jVC.size(); i++)
            {
                // if ( abs(robot_state.joint_position[i]+ dt*jVC[i]) > speedLims[i])
                // {
                //     //jVC[i] = speedLims[i];
                //     if (i!= 2)
                //     {
                //     jVC[i] = 0.0;
                //     }
                //     else
                //     {
                //         // do nothing for last
                //         //jVC[i] = speedLims[i];
                //     }
                //     // if (mainTaskType[mTChoice]== "articular")
                //     // {
                //     //     std::cout << "bug : too big speed" << std::endl;
                //     // }
                // }

                jsc_msg.velocity.push_back(jVC[i]);
                jsc_msg.position.push_back(robot_state.joint_position[i]+ dt_val*jVC[i]);
                
                //jsc_msg.velocity.push_back(jVC[nJ-i-1]);
                //jsc_msg.position.push_back(robot_state.joint_position[nJ-i-1]+ period*jVC[nJ-i-1]);


                // if (i==0)
                // {
                //     std::cout << robot_state.joint_position[i] << std::endl;
                //     std::cout  << jsc_msg.position[i] << std::endl;
                //     std::cout << "===" << std::endl;

                // }
            }

        }
    }

    void VelocityController::getCartCommand(const msg_srv_action_gestion::poseVelAcc::ConstPtr& msg)
    {

        if (trackingAuthorized)
        {
            Eigen::Quaternion<double> q(msg->pose.orientation.w,
            msg->pose.orientation.x,
            msg->pose.orientation.y,
            msg->pose.orientation.z       
            );
            Eigen::Matrix3d rotationMatrix = q.matrix();
            Eigen::Vector3d translationMatrix (msg->pose.position.x, 
            msg->pose.position.y, 
            msg->pose.position.z);

            lastDesiredCartesianPose.setIdentity();
            lastDesiredCartesianPose.block<3,3>(0,0) = rotationMatrix;
            lastDesiredCartesianPose.block<3,1>(0,3) = translationMatrix;

            lastDesiredCartesianVelocity[0,0] = msg->twist.linear.x;
            lastDesiredCartesianVelocity[1,0] = msg->twist.linear.y;
            lastDesiredCartesianVelocity[2,0] = msg->twist.linear.z;
            lastDesiredCartesianVelocity[3,0] = msg->twist.angular.x;
            lastDesiredCartesianVelocity[4,0] = msg->twist.angular.y;
            lastDesiredCartesianVelocity[5,0] = msg->twist.angular.z;                                

        }                        

    }



    void VelocityController::publishInfos(  sensor_msgs::JointState cjs_msg,  sensor_msgs::JointState jsc_msg)
    {

        ros::Time ts= ros::Time::now();

        cjs_msg.header.stamp = ts;

        jsc_msg.header.stamp = ts;

        // send current position (from controller)


        QP_error_pub.publish(error_msg);

        if (isSim)
        {

            joint_state_publisher.publish(cjs_msg);
            if (!withGazebo)
            {
                // send desired position 
                command_state_publisher.publish(jsc_msg);
                // without gazebo, we use own desired command as current state
                jsr_msg = jsc_msg;
            }
            else
            {
                // send desired velocity command
                std_msgs::Float64MultiArray msg;
                Eigen::Matrix<double,3,1> joint_command;
                for (int i = 0; i < jsc_msg.velocity.size(); i++)
                {   
                    joint_command[i]  = jsc_msg.velocity[i];
                }
                tf::matrixEigenToMsg(joint_command,msg);                
                joint_velocity_command_pub.publish(msg);
            }
        }
        else
        {
            //ROS_INFO("HERE");
            // send desired velocity command
            command_state_publisher.publish(jsc_msg);
        }
    
        

        ros::spinOnce();

    }

/*

jsr : joint_state_received

*/
    void VelocityController::getCurrentJointState(const sensor_msgs::JointState& msg)

    {
    //    std::cout << "got current state" << std::endl;

    //    for (int i = 0; i < msg.position.size(); i++)
    //    {
    //         ROS_INFO("Joint %s : pos : %3.4f; vel : %3.4f", msg.name[i].c_str(), msg.position[i], msg.velocity[i]);
    //    }


       if (!isSim)
       {
        int nJ = model->getNrOfDegreesOfFreedom();

        // little bug corrected here
        for (int i = 0; i < msg.position.size(); i++)
        {
            jsr_msg.position[i] = msg.position[i];
            jsr_msg.velocity[i] = msg.velocity[i]; 
            jsr_msg.effort[i] = msg.effort[i]; 

        }
        

        // TODO CHECK IF OK WITH THAT
        // jsr_msg.position = msg.position;
        // jsr_msg.velocity = msg.velocity;
        // jsr_msg.effort = msg.effort;



       }
        else
        {
           jsr_msg.position = msg.position;
           jsr_msg.velocity = msg.velocity;
           jsr_msg.effort = msg.effort;

        }


       gotState = true;
    }

    void VelocityController::getJointCommand(const sensor_msgs::JointState& msg)
    {
        if (trackingAuthorized)
        {
            // ok
            //ROS_INFO("Got joint command!");

            for (int i = 0; i < msg.position.size(); i++)
            {
                lastDesiredJointConfig[i] = msg.position[i];
            }

            for (int i = 0; i < msg.velocity.size(); i++)
            {
                lastDesiredJointSpeed[i] = msg.velocity[i];
            }

            //std::cout << lastDesiredJointConfig << std::endl;

        }


    }

    bool VelocityController::setRegTask(msg_srv_action_gestion::DoubleFloat64MultiArraySrv::Request & req,
     msg_srv_action_gestion::DoubleFloat64MultiArraySrv::Response & res)
    {
        double val = req.input.data[0];
        int valInt = (int)val;

        res.output.data.push_back(regChoice);

        regChoice = valInt;

        ROS_INFO("Reg task type taken in QP : %s", regPoss[regChoice].c_str());

        //resetQP = true;
        resetReg = true;
        res.success = true;


        return(true);
    }

    bool VelocityController::setControlType(msg_srv_action_gestion::SetFloat::Request & req,
     msg_srv_action_gestion::SetFloat::Response & res)
    {
        double val = req.data.data;
        int valInt = (int)val;
        chosenMainTask = valInt;

        ROS_INFO("Command type taken in QP : %s", mainTaskType[chosenMainTask].c_str());

        res.success = true;
        return(true);
    }

    bool  VelocityController::setTrackMvt(std_srvs::SetBool::Request & req,
     std_srvs::SetBool::Response & res)
    {
        trackingAuthorized = req.data;
        res.success = true;
        res.message = "tracking set";
        return(true);
    }

    /*
    
    Send as following list : [x,y,z, rx,ry,rz,rw]
    
    */
    bool  VelocityController::sendPoseInit(msg_srv_action_gestion::DoubleFloat64MultiArraySrv::Request & req,
     msg_srv_action_gestion::DoubleFloat64MultiArraySrv::Response & res)
    {
        std::vector<double> vals = tU.toL(poseInit);

        std_msgs::MultiArrayDimension dim;
        dim.size = 7;

        for (int i = 0; i < vals.size(); i++)
        {
            res.output.data.push_back(vals[i]);
        }
        res.output.layout.dim.push_back(dim);
        res.success = true;

        return(true);
    }

    bool  VelocityController::isQPInitialized(msg_srv_action_gestion::GetBool::Request & req,
     msg_srv_action_gestion::GetBool::Response & res)
    {
        res.data = initialized;
        res.success = true;

        return(true);

    }
}