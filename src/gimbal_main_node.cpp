#include <gimbal_gazebo_qontrol/gimbal_qontrol.h>


using namespace gimbal_qp_controller;

int main(int argc, char **argv)
{
    ros::init(argc, argv, "gimbal_qp_control");
    ROS_WARN_STREAM_ONCE("Starting main GIMBAL QP Controller");

    ros::NodeHandle n("~");

    bool withGazebo;
    std::string nodeName = ros::this_node::getName();
    nodeName += "/";

    double rate;
    double horizon_dt;


    //rosWrap::getRosParam(nodeName+"rate", rate);
    rosWrap::getRosParam("/loop_rate", rate);

    // time horizon for computing QP
    rosWrap::getRosParam(nodeName + "horizon_dt", horizon_dt);

    rosWrap::getRosParam(nodeName + "withGazebo", withGazebo);

    std::string urdfPath;

    rosWrap::getRosParam(nodeName + "urdfPath", urdfPath);

    bool mainLocal;
    rosWrap::getRosParam(nodeName + "mainTaskLocal", mainLocal);

    std::string model_name = "gimbal";

    std::cout << "withGz : " << withGazebo << std::endl;
    std::cout << 1.0/rate << std::endl;

    ros::Rate loop_rate(rate);
    VelocityController vel(n,horizon_dt,urdfPath,withGazebo);

    vel.mainTaskLocal = mainLocal;
    if (withGazebo)
    {
        // Hack to wait for gazebo to be ready before initiating the robot state
        ros::ServiceClient gazebo_client = n.serviceClient<gazebo_msgs::GetPhysicsProperties>("/gazebo/get_physics_properties");
        ROS_WARN_STREAM_ONCE("Waiting for gazebo to be ready");
        gazebo_client.waitForExistence();
        std::this_thread::sleep_for(std::chrono::milliseconds(5000)); // sleep to wait for joint to be in correct configuration 
        ROS_WARN_STREAM_ONCE("Gazebo ready. Wait for q_init");


        while (ros::ok() && !vel.gotState )
        {
            ros::spinOnce();
        }

        ROS_WARN_STREAM_ONCE("Initialization with Gazebo done.");
    }
    else
    {
        // just load q_init from file
        // get q_init
        Eigen::VectorXd set_q_init;
        set_q_init.resize(3);
        rosWrap::getRosParam("/gimbal/velocity_control/q_init", set_q_init);        

        for (int i = 0; i < set_q_init.size(); i++)
        {
            vel.lastDesiredJointConfig[i] = set_q_init[i];
            vel.lastDesiredJointSpeed[i] = 0.0;
        }

        vel.chosenMainTask = 1;
    }



    vel.initialize();





    while(ros::ok())
    {
        //ROS_INFO("Here");
        ros::spinOnce();
        vel.update();


        loop_rate.sleep();
    }
    return 0;
}