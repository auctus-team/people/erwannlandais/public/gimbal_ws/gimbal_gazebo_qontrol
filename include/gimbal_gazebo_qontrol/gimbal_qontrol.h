#pragma once

#include "pinocchio/fwd.hpp"
#include <ros/ros.h>
#include <ros/node_handle.h>
#include <ros/time.h>

#include <ros/package.h>

#include "sensor_msgs/JointState.h"
#include "gazebo_msgs/GetPhysicsProperties.h"
// #include <moveit_trajectory_interface/moveit_trajectory_interface.hpp>
#include <chrono>
#include <thread>

#include "Qontrol/Qontrol.hpp"
#include "ros_wrap/rosparam_getter.h"
#include "tf_management_pkg/tf_utils.h"
#include "msg_srv_action_gestion/poseVelAcc.h"
#include "msg_srv_action_gestion/GetBool.h"
#include "msg_srv_action_gestion/SetFloat.h"
#include "msg_srv_action_gestion/DoubleFloat64MultiArraySrv.h"
#include "std_srvs/SetBool.h"

#include "std_msgs/Float64MultiArray.h"
#include "eigen_conversions/eigen_msg.h"
using namespace Qontrol;

namespace gimbal_qp_controller {

class VelocityController 
{
    public:

        VelocityController(ros::NodeHandle& n, double period, std::string urdfPath, bool withGazebo = true);

        void init_qp();

        bool initialize();
        void update();
        void updateQP(Eigen::Matrix4d desiredCartesianPose, Eigen::Matrix<double,6,1> desiredCartesianVelocity, Eigen::VectorXd qdes,
    Eigen::VectorXd qd_des,
    double dt);
        void getCartCommand(const msg_srv_action_gestion::poseVelAcc::ConstPtr& msg);
        void getJointCommand(const sensor_msgs::JointState& msg);
        void publishInfos(  sensor_msgs::JointState cjs_msg,  sensor_msgs::JointState jsc_msg);
        void getCurrentJointState(const sensor_msgs::JointState& msg);

        void manageROSCalls();
        
        bool setControlType(msg_srv_action_gestion::SetFloat::Request & req,
            msg_srv_action_gestion::SetFloat::Response & res);
        bool isQPInitialized(msg_srv_action_gestion::GetBool::Request & req,
           msg_srv_action_gestion::GetBool::Response & res);
        bool sendPoseInit(msg_srv_action_gestion::DoubleFloat64MultiArraySrv::Request & req,
          msg_srv_action_gestion::DoubleFloat64MultiArraySrv::Response & res);
        bool setTrackMvt(std_srvs::SetBool::Request & req,
          std_srvs::SetBool::Response & res);

        bool setRegTask(msg_srv_action_gestion::DoubleFloat64MultiArraySrv::Request & req,
            msg_srv_action_gestion::DoubleFloat64MultiArraySrv::Response & res);



        void QPRecovery(Eigen::VectorXd & jVC, Eigen::Matrix4d desiredCartesianPose, Eigen::Matrix<double,6,1> desiredCartesianVelocity, Eigen::VectorXd qdes,
    Eigen::VectorXd qd_des,
    double dt);

        // joint state commanded (directly sent to servoMotors)
        sensor_msgs::JointState jsc_msg;
        // current joint_state (in controller)
        sensor_msgs::JointState js_msg; 
        // joint state received
        sensor_msgs::JointState jsr_msg; 
        bool gotState;

        bool qpSuccess = true;

        Eigen::VectorXd lastSavedConfigForRecover;


        // last desired cartesian pose / velocity
        Eigen::Matrix4d lastDesiredCartesianPose;
        Eigen::Matrix<double,6,1> lastDesiredCartesianVelocity;

        Eigen::VectorXd lastDesiredJointConfig;
        Eigen::VectorXd lastDesiredJointSpeed;
        int chosenMainTask = -1;
        double distQPRec;

        bool mainTaskLocal = false;

        double gamma = 0.01;


        double simu_time_factor = 1.0;

    private:

        tfUtils tU;

        ros::NodeHandle nh;

        bool trackingAuthorized = false;


        bool initialized = false;

        Eigen::Vector3d upperQBounds;
        Eigen::Vector3d lowerQBounds;

        Eigen::VectorXd speedLims;

        Eigen::VectorXd accLimsOri;

        Eigen::VectorXd accLims;

        // From q_init, get poseInit (expressed in base frame of robot)
        Eigen::Matrix4d poseInit;


        double maxSpeedPercent;

        double maxAccPercent;
        
        // QP elements

        std::string controlled_frame;

        std::string base_frame;

        std::string urdf_path;

        std::shared_ptr<Qontrol::Model::RobotModel<Qontrol::Model::RobotModelImplType::PINOCCHIO>> model;
        std::shared_ptr<Qontrol::JointVelocityProblem> velocity_problem;
        std::shared_ptr<Qontrol::Task::GenericTask> main_task; 

        std::shared_ptr<Qontrol::Task::GenericTask> config_reg_task_cob; 
        std::shared_ptr<Qontrol::Task::GenericTask> config_reg_task_vM; 
        
        
        
        // std::shared_ptr<Qontrol::Task::CartesianVelocity<Qontrol::ControlOutput::JointVelocity>> main_task;         

        std::shared_ptr<Qontrol::Constraint::GenericConstraint> joint_configuration_constraint ;

        std::shared_ptr<Qontrol::Constraint::GenericConstraint> joint_velocity_constraint;
        
        std::shared_ptr<Qontrol::Constraint::GenericConstraint> joint_acceleration_constraint;

        //std::shared_ptr<Qontrol::Constraint::GenericConstraint> joint_jerk_constraint;

        Qontrol::RobotState robot_state;

        double cob_task_weight;
        double vM_task_weight;

        bool resetQP = false;

        bool resetReg = false;


        double saved_cob_task_weight;

        double saved_vM_task_weight;
        // ROS elements

        bool isSim;
        

        ros::Publisher joint_state_publisher;
        ros::Publisher command_state_publisher;

        ros::Subscriber joint_state_subscriber;
        ros::Subscriber cartesian_command_subscriber;
        ros::Subscriber joint_command_subscriber;

        ros::Publisher joint_velocity_command_pub;

        ros::Publisher QP_error_pub;

        ros::ServiceServer switchMainTaskServer;


        msg_srv_action_gestion::poseVelAcc error_msg;


        sensor_msgs::JointState template_js_msg;


        double period;


        Eigen::Matrix<double,3,1> pGainsJ;
        Eigen::Matrix<double,3,1> pGainsC;
        bool withGazebo;

        std::vector<std::string> regPoss = {"VelocityMinimisation", "MiddleOfConfigBounds"};
        int regChoice;

        std::vector<std::string> mainTaskType = {"cartesian","articular"};
        int mTChoice;

        ros::ServiceServer switchControlServer;


        ros::ServiceServer switchRegTaskServer;


        ros::ServiceServer poseInitServer;

        ros::ServiceServer trackMvtServer;

        ros::ServiceServer initializedServer;


};

}