# Description

At first, the goal of this package was to interface the control code of the robot through Gazebo, in order to get a more realistic simulation of the robot and be able to simulate various sensors on it (camera for example).

However, after several attempts, the time required to set up such an interface led us to abandon this idea and start with a very basic simulation of the robot (direct application of QP instructions to the robot's movement). Some of the ideas explored are presented at the end of the Readme.

From now on, this package is dedicated to the application of a QP for robot control, in simulation and in reality, via the “gimbal_qontrol.cpp” file. It is possible to change the regularization task (center of joint stops, speed minimization) and the main task (Cartesian tracking, joint tracking) during task execution, via rosservice calls.

## Ideas to interface controller with Gazebo

Ideas to use a controller through gazebo plugin : 

https://classic.gazebosim.org/tutorials?tut=ros_control (urdf format; maybe not useful)

https://github.com/PickNikRobotics/ros_control_boilerplate/tree/noetic-devel (generic interface; in a way, is equivalent to  franka_hw::FrankaStateInterface or franka_hw::FrankaModelInterface)

https://gitlab.inria.fr/auctus/panda/torque-qp-extended/torque_qp/-/blob/7d8ccca9e580a983bd03ab86fcef44b0e4ff4bf5/src/robot/panda_simulation.cpp (direct communication through Gazebo world)